# Tutos Kmymoney

![logo](kmymoney.png)

Ce dépot regroupe les documents utilisés lors de la création d'une [série de vidéos](https://peertube.iriseden.eu/video-channels/kmymoney/videos) visant à expliquer les bases de la finance personnelle et le fonctionnement de [Kmymoney](https://kmymoney.org/).

  * Pourquoi faire ces comptes ? (Partie 1)
  * Première prise en main de Kmymoney (Partie 2)
  * Mise en place des comptes (Partie 3)
  * Utilisation de Kmymoney au quotidien (Partie 4)
  * Pointer et rapprocher (Partie 5)
  * Les Étiquettes (Partie 6)
  * Import d'opérations (Partie 7)
  * Budget (Partie8)
  * Opérations récurrentes et prévisions (Partie 9)

Pour toutes questions ou commentaires, n'hésitez pas à vous rendre sur le forum [Framacolibri](https://framacolibri.org/t/tutos-kmymoney/8620).  

[Lien vers la chaîne peertube non officielle de Kmymoney](https://peertube.iriseden.eu/video-channels/kmymoney/videos)  
[Site officiel de Kmymoney](https://kmymoney.org/)  
[Faites un don](https://kde.org/community/donations/)